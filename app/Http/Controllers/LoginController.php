<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
    public function logUserIn() {
    	try {
			$request = json_decode(Request()->getContent());
			$username = $request->username;
			$password = $request->password;

			if ($username=="salman" && $password=="1234") {
				$code = 101;
				$message = "All Ok";
				$userId = 23;
			} else {
				$code = 102;
				$message = "Invalid Username/ Password.";
				$userId = -1;
			}

    	} catch (\Exception $e) {
			$code = 104;
			$message = "Some error occured, please try again later.";
    	}
		return response()->json(['code' => $code, 'message' => $message, 'id' => $userId], 200);
    }
}
